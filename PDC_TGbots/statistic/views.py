from django.views import View
from django.shortcuts import render, redirect
from users.models import User
from order.models import Order

class un_model_user():
    def __init__(self, num, nickname, coins):
         self.num = num
         self.nickname = nickname
         self.coins = coins

class Index(View):
    def get(self, request):
        if request.user.is_authenticated:
            if request.user.is_admin():
                users_list = []
                final_list = []
                for usr in User.objects.all():
                    users_list.append(
                        (usr.username, usr.coins)
                    )
                sorted_users_list = sorted(
                                users_list, key=lambda item: item[1]
                            )[::-1]
                
                for usr in sorted_users_list:
                    final_list.append(
                        un_model_user(sorted_users_list.index(usr) + 1, usr[0], usr[1])
                    )
                users_list = User.objects.all()
                all_orders_col = len(Order.objects.all())
                all_orders_col_day = 0 
                all_orders_col_week = 0
                for x in Order.objects.all():
                    if x.is_new_day():
                        all_orders_col_day += 1
                for x in Order.objects.all():
                    if x.is_new_week():
                        all_orders_col_week += 1
                return render(request, "statistic/index.html", {
                        "col": all_orders_col, 
                        "col_day": all_orders_col_day, 
                        "col_week": all_orders_col_week,
                        "users": final_list
                    }
                )
            else:
                return render(request, "not.html", {"message": "Скорее всего вы не являетесь администратором..."})
        else:
            return redirect("/")