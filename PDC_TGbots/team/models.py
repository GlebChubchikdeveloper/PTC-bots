from django.db import models

class Submino(models.Model):
    username = models.CharField("Username", max_length=100)
    text = models.TextField("Текст")
    def __str__(self):
        return self.username