from django.urls import path
from . import views

appname = "team"
urlpatterns = [
    path("", views.Index.as_view(), name="index"),
    path("send_request", views.SendRequest.as_view(), name="submino"),
    path("gleb-chubchik/", views.GlebChubchik.as_view(), name="gleb_chubchik"),
    path("ivan-shneider/", views.IvanShneider.as_view(), name="ivan_shneider"),
]
