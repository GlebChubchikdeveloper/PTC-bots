from django.shortcuts import redirect, render
from django.views import View
from .models import Submino

class Index(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "team/index.html")
        else:
            return redirect("/")

class GlebChubchik(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "team.gleb_chubchik/index.html")
        else:
            return redirect("/")

class IvanShneider(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "team.ivan_shneider/index.html")
        else:
            return redirect("/")

class SendRequest(View):
    def post(self, request):
        submino = request.POST.get("submino")
        username = request.POST.get("username")
        Submino.objects.create(username=str(username), text=str(submino))
        return redirect("/team")