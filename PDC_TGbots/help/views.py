from django.views import View
from django.shortcuts import render, redirect

class Index(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "help/index.html")
        else:
            return redirect("/")

class WhatIMustDoingIfIDontKnowHard(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "help/what_i_must_doing_if_i_dont_know_hard.html")
        else:
            return redirect("/")

class HowBeAdmin(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "help/how_be_admin.html")
        else:
            return redirect("/")

class HowSeeMyOrder(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "help/how_be_admin.html")
        else:
            return redirect("/")

class WhyIGoToNotPage(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "help/why_i_go_to_notpage.html")
        else:
            return redirect("/")