from django.urls import path
from . import views

urlpatterns = [
    path("", views.Index.as_view(), name="index"),
    path("what-i-must-doing-if-i-dont-know-hard", views.WhatIMustDoingIfIDontKnowHard.as_view()),
    path("how-be-admin", views.HowBeAdmin.as_view()),
    path("how-see-my-order", views.HowSeeMyOrder.as_view()),
    path("why-i-go-to-notpage", views.WhyIGoToNotPage.as_view()),
]
