from django.db import models
from users.models import User

class Correspondence(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class CorrespondenceMessage(models.Model):
    username = models.CharField(max_length=200)
    correspondence = models.ForeignKey(Correspondence, on_delete=models.CASCADE)
    message = models.TextField("Текст")

