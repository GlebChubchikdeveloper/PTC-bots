from django.shortcuts import redirect, render
from django.views import View
from users.models import User
from .models import Correspondence, CorrespondenceMessage

class Admin(View):
    def get(self, request):
        if request.user.is_authenticated:
            if request.user.is_admin():
                _all = Correspondence.objects.all()
                return render(request, "support/admin.html", {"all": _all})
            else:
                return render(request, "not.html", {"message": "Скорее всего вы не явлеятесь администратором"})
        else:
            return redirect("/")

class Detail(View):
    def get(self, request, _id):
        if request.user.is_authenticated:
            try:
                user = User.objects.order_by("id")[_id - 1]
                all_correspondences = Correspondence.objects.all()
                for _correspondence in all_correspondences:
                    if _correspondence.user == user:
                        correspondence = _correspondence
                        break
            except Exception:
                return render(request, "404.html")
            try:
                correspondence = _correspondence = Correspondence.objects.get(id = _id)
            except Exception:
                correspondence = Correspondence.objects.create(user=request.user)
            messages = correspondence.correspondencemessage_set.order_by("id")
            if request.user.is_admin() or request.user == correspondence.user:
                return render(request, "support/detail.html", {"correspondence": correspondence, "messages": messages})
            else:
                return render(request, "not.html", {"message": "Скорее всего вы не явлеятесь администратором"})
        else:
            return redirect("/")

class LeaveMessage(View):
    def post(self, request, _id):
        if request.user.is_authenticated:
            _correspondence = Correspondence.objects.get(id = _id)
            CorrespondenceMessage.objects.create(username=request.user, correspondence=_correspondence, message=request.POST.get("message_text"))
            messages = _correspondence.correspondencemessage_set.order_by("id")
            return redirect(f"/support/{_id}", {"correspondence": _correspondence, "messages": messages})
        else:
            return redirect("/")