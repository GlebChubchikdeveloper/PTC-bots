from django.urls import path
from . import views

urlpatterns = [
    path("", views.Admin.as_view(), name="support_admin"),
    path("<int:_id>/", views.Detail.as_view(), name="support_detail"),
    path("<int:_id>/leave_message", views.LeaveMessage.as_view(), name="support_leave_message"),
]
