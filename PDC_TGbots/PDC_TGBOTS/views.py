from django.shortcuts import render

def page_not_found_view(request, exception):
    return render(request, '404.html', status=404)

def _handler500(request):
    return render(request, '403-500_errors.html', status=500)