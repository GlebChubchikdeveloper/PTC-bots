from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("", include("home.urls")),
    path("help/", include("help.urls")),
    path("users/", include("users.urls")),
    path("order-a-bot/", include("order.urls")),
    path("team/", include("team.urls")),
    path("grappelli/", include("grappelli.urls")),
    path("admin-chat/", include("adminchat.urls")),
    path("all-requests/", include("all_requets.urls")),
    path('support/', include("support.urls")),
    path('statistic/', include("statistic.urls")),
    path('administration/', admin.site.urls),
]

handler404 = "PDC_TGBOTS.views.page_not_found_view"
handler500 = "PDC_TGBOTS.views._handler500"
