from django.urls import path
from . import views

appname = "adminchat"
urlpatterns = [
    path("", views.Index.as_view(), name="index"),
    path("leave_message/", views.LeaveMessage.as_view(), name="leave_message")
]