from re import M
from django.contrib.admin import ModelAdmin, register
from .models import AdminMessage

@register(AdminMessage)
class AdminMessageAdmin(ModelAdmin):
    pass
