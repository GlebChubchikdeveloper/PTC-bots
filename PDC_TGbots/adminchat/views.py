from django.views import View
from django.shortcuts import render, redirect
from .models import AdminMessage

class Index(View):
    def get(self, request):
        if request.user.is_authenticated:
            if request.user.is_admin():
                messages = AdminMessage.objects.all()
                return render(request, "adminchat/index.html", {"messages": messages})
            else:
                return render(request, "not.html", {"message": "Скорее всего вы не являетесь администратором..."})
        else:
            return redirect("/")

class LeaveMessage(View):
    def post(self, request):
        if request.user.is_authenticated:
            if request.user.is_admin():
                messages = AdminMessage.objects.all()
                message_text = request.POST.get("message_text")
                AdminMessage.objects.create(user=request.user, message_text=message_text)
                return redirect("/admin-chat", {"messages": messages})
            else:
                return render(request, "not.html", {"message": "Скорее всего вы не являетесь администратором..."})
        else:
            return redirect("/")