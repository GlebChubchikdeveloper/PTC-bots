from email.mime import message
from django.db import models
from users.models import User

class AdminMessage(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message_text = models.TextField("Сообщение")
    def __str__(self):
        return f"сообщение: {self.message_text}"
    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"