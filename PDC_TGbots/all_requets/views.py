from django.views import View
from django.shortcuts import render
from team.models import Submino

class Index(View):
    def get(self, request):
        if request.user.is_authenticated:
            if request.user.is_admin():
                list = Submino.objects.order_by("-id")
                return render(request, "all_requests/index.html", {"list": list})
            else:
                return render(request, "not.html", {"message": "Похоже вы не являетесь администратором..."})
        else:
            return redirect("/")

class Detail(View):
    def get(self, request, req_id):
        if request.user.is_authenticated:
            if request.user.is_admin():
                try:
                    req = Submino.objects.order_by("id")[req_id - 1]
                    return render(request, "all_requests/detail.html", {"req": req})
                except Exception:
                    return render(request, "404.html")
            else:
                return render(request, "not.html", {"message": "Похоже вы не являетесь администратором..."})
        else:
            return redirect("/")