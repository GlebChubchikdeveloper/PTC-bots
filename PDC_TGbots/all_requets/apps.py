from django.apps import AppConfig


class AllRequetsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'all_requets'
