from django.db.utils import IntegrityError
from django.views import View
from django.views.generic import TemplateView
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.contrib.auth.hashers import make_password
from django.shortcuts import redirect, render
from .models import User

class Register(TemplateView):
    template_name = "registration/register.html"

class AsRegister(View):
    def post(self, request):
        username = request.POST.get("username").lower()
        email = request.POST.get("email")
        password = request.POST.get("password")
        password2 = request.POST.get("password2")
        # Check
        checked = bool(password == password2)
        password_check = bool(len(password) >= 8)
        username_is_free = True
        if checked == False or password_check == False:
            errors = True
        elif checked == True and password_check == True:
            try:
                User.objects.create(username=username, email=email, coins=0, password=make_password(password), lang="english", lang_has_changed=False)
                username_is_free = True
                errors = False
            except IntegrityError:
                errors = True
                username_is_free = False
        return render(request, "registration/as_register.html", {
            "username": username,
            "username_is_free": username_is_free == False,
            "password_check": password_check == False,
            "checked": checked == False,
            "errors": errors
        })

class Login(TemplateView):
    template_name = "registration/login.html"

class AsLogin(View):
    def post(self, request):
        username = request.POST.get("username").lower()
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        if user != None:
            login(request, user)
            user_is = True
            return redirect("/", foo="bar")
        else:
            user_is = False
            template = "registration/as_login.html"
            return render(request, template, {"user_is": user_is == False})