# Generated by Django 4.1 on 2022-09-29 14:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='coins',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
