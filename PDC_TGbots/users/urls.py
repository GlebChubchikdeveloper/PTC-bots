from django.urls import path, include
from users.views import AsRegister, Register, AsLogin, Login

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('register/', Register.as_view(), name='register'),
    path('login/', Login.as_view(), name='register'),
    path("as_register/", AsRegister.as_view(), name="as_register"),
    path("as_login", AsLogin.as_view(), name="as_login")
]
