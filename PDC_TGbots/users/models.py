from django.contrib.auth.models import AbstractUser
from django.db import models

admins_list = [
    "gleb chubchik | админ",
    "ivan shneider | админ"
]
class User(AbstractUser):
    coins = models.IntegerField()
    lang = models.CharField(max_length=100)
    lang_has_changed = models.BooleanField()

    def is_admin(self):
        return True if self.username in admins_list else False
