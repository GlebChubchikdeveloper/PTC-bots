from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

class Loginform(AuthenticationForm):
    username = forms.CharField(label="Имя пользователя", widget=forms.TextInput(attrs={"class": "form-django"}))
    password = forms.CharField(label="Пароль", widget=forms.PasswordInput(attrs={"class": "form-django"}))