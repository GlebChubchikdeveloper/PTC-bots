from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import TemplateView
from users.models import User
from order.models import Order

class Index(TemplateView):
    template_name = "home/index.html"

class Account(View):
    def get(self, request):
        if request.user.is_authenticated:
            day, week, all_orders = 0, 0, 0
            orders_list_day = []
            orders_list_week = []
            for order in Order.objects.all():
                if order.is_new_day():
                    orders_list_day.append(order)
            for order in Order.objects.all():
                if order.is_new_week():
                    orders_list_week.append(order)
            for order in orders_list_day:
                if order.user == request.user:
                    day += 1
            for order in orders_list_week:
                if order.user == request.user:
                    week += 1
            for order in Order.objects.all():
                if order.user == request.user:
                    all_orders += 1
            return render(request, "account/index.html", {
                    "username": request.user.username.title(), 
                    "all_orders": all_orders,
                    "day": day,
                    "week": week
                }
            )
        else:
            return redirect("/")

class Shop(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "shop/index.html")
        else:
            return redirect("/")



class Buy(View):
    def get(self, request, coins):
        if request.user.is_authenticated:
            request.user.coins += coins
            request.user.save()
            return redirect("/shop/")
        else:
            return redirect("/")

class MyProblems(View):
    def get(self, request):
        if request.user.is_authenticated:
            if request.user.is_admin():
                return render(request, "problems/index.html")
            else:
                return render(request, "not.html", {"message": "Скорее всего вы не являетесь администратором..."})
        else:
            return redirect("/")

class CloseLanguageMessage(View):
    def get(self, request):
        if request.user.is_authenticated:
            request.user.lang_has_changed = True
            request.user.save()
        return redirect("/")

class ChangeLanguage(View):
    def get(self, request, language):
        if request.user.is_authenticated:
            request.user.lang = language
            request.user.lang_has_changed = True
            request.user.save()
        return redirect("/")