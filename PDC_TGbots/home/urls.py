from django.urls import path
from . import views

appname = "home"
urlpatterns = [
    path("", views.Index.as_view(), name="index"),
    path("close-language-message", views.CloseLanguageMessage.as_view(), name="close-lang-message"),
    path("change-language/<str:language>", views.ChangeLanguage.as_view(), name="change-language"),
    path("account/", views.Account.as_view(), name="my-account"),
    path("shop/", views.Shop.as_view(), name="shop"),
    path("shop/buy/<int:coins>", views.Buy.as_view(), name="buy"),
    path("problems", views.MyProblems.as_view(), name="my_problems"),
]