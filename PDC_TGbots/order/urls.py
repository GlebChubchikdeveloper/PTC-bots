from django.urls import path
from . import views

app_name = "order"
urlpatterns = [
    path("", views.Index.as_view(), name="index"),
    path("my-orders", views.MyOrders.as_view(), name="my-orders"),
    path("<int:order_id>", views.Detail.as_view(), name="detial"),
    path("<int:order_id>/leave-message", views.LeaveMessage.as_view(), name="leave_message"),
    path("leave-order", views.LeaveOrder.as_view(), name="leave_order"),
    path("<int:_order>/cancel", views.Cancel.as_view(), name="cancel"),
    path("<int:_order>/set_code", views.SetCode.as_view(), name="set_code"),
    path("<int:_order>/buy", views.Buy.as_view(), name="buy"),
    path("<int:_order>/set_price", views.SetPrice.as_view(), name="set_price"),
    path("<int:_order>/download", views.DownloadCode.as_view(), name="download")
]
