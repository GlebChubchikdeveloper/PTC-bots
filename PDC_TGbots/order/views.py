from django.shortcuts import redirect, render
from django.views import View
from .models import Message, Order
from datetime import datetime
from email.mime.text import MIMEText
from users.models import User
import smtplib

def send_email(message, subject):
    sender = "gleb.chubchik2@gmail.com"
    password = "idpvetzacqlaaoej"
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.starttls()
    try:
        server.login(sender, password)
        msg = MIMEText(message)
        msg["Subject"] = subject
        server.sendmail(sender, sender, msg.as_string())
        server.sendmail(sender, "ivansnejder27@gmail.com", msg.as_string())
    except Exception:
        pass

class Index(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "order/index.html")
        else:
            return redirect("/")

class MyOrders(View):
    def get(self, request):
        if request.user.is_authenticated:
            user = request.user
            if user.is_admin() == False:
                no_list = []
                yes_list = []
                ready_list = []
                orders_list = Order.objects.all()
                for order in orders_list:
                    if order.user == user:
                        if order.state == "Ready":
                            ready_list.append(order)
                        elif order.state == "True":
                            yes_list.append(order)
                        else:
                            no_list.append(order)
                return render(request, "my_orders/index.html", {"yes_list": yes_list[::-1], "no_list": no_list[::-1], "ready_list": ready_list[::-1]})
            else:
                no_list = []
                yes_list = []
                ready_list = []
                orders_list = Order.objects.all()
                for order in orders_list:
                    if order.state == "Ready":
                        ready_list.append(order)
                    elif order.state == "True":
                        yes_list.append(order)
                    else:
                        no_list.append(order)
                return render(request, "my_orders/index.html", {"yes_list": yes_list[::-1], "no_list": no_list[::-1], "ready_list": ready_list[::-1], "last_order": Order.objects.last()})
        else:
            return redirect("/")

class Detail(View):
    def get(self, request, order_id):
        if request.user.is_admin() or request.user.username == Order.objects.order_by("id")[order_id - 1].user.username:
            try:
                _order = Order.objects.get(id = order_id)
            except Exception:
                return render(request, "404.html")
            messages = _order.message_set.order_by("id")
            return render(request, "my_orders/detail.html", {"order": _order, "messages": messages})
        else:
            return render(request, "not.html", {"message": "Скорее всего это не ваш заказ..."})

class LeaveMessage(View):
    def post(self, request, order_id):
        try:
            _order = Order.objects.get(id = order_id)
        except Exception:
            return render(request, "404.html")
        message_text = request.POST.get("message_text")
        # Creating a message
        Message.objects.create(user=request.user, order=_order, message_text=message_text)
        # updating a message-list
        messages = _order.message_set.order_by("id")
        return redirect(f"/order-a-bot/{order_id}", {"order": _order, "messages": messages})

class LeaveOrder(View):
    def post(self, request):
        Order.objects.create(
                            user=request.user,
                            order_title=request.POST.get("order_title"),
                            order=request.POST.get("order_text"),
                            power=request.POST.get("power"),
                            order_date=datetime.now(),
                            price = 0,
                            state = "False",
                            code = "https://staticfiles-com.netlify.app/static/files/code.zip",
        )
        last_order = Order.objects.last()
        message = f'Сыллка на заказ: "https://botsptc.pythonanywhere.com/order-a-bot/{last_order.id}" \nТекст заказа: {request.POST.get("order_text")}   \n\nДата заказа: {datetime.now()}'
        subject = f'Новый заказ: {request.POST.get("order_title")}'
        send_email(message=message, subject=subject)
        return redirect("/order-a-bot")

class Cancel(View):
    def get(self, request, _order):
        __order =  Order.objects.get(id = _order)
        if request.user.is_admin():
            __order.state = "False"
            __order.save()
        messages = __order.message_set.order_by("id")
        return redirect(f"/order-a-bot/{_order}", {"order": __order, "messages": messages})

class SetCode(View):
    def post(self, request, _order):
        __order =  Order.objects.get(id = _order)
        if request.user.is_admin():
            __order.code = request.POST.get("code")
            __order.state = "True"
            __order.save()
        messages = __order.message_set.order_by("id")
        return redirect(f"/order-a-bot/{_order}", {"order": __order, "messages": messages})

class Buy(View):
    def get(self, request, _order):
        __order = Order.objects.get(id = _order)
        if __order.state == "True":
            __order.state = "Ready"
            __order.save()
            request.user.coins -= int(__order.price)
            request.user.save()
        return redirect(f"/order-a-bot/{_order}/download")

class SetPrice(View):
    def post(self, request, _order):
        __order = Order.objects.get(id = _order)
        if request.user.is_admin():
            __order.price = int(request.POST.get("price"))
            __order.save()
        messages = __order.message_set.order_by("id")
        return redirect(f"/order-a-bot/{_order}", {"order": __order, "messages": messages})

class DownloadCode(View):
    def get(self, request, _order):
        __order = Order.objects.get(id = _order)
        if __order.state == "Ready":
            return render(request, "my_orders/download.html", {"order": __order})
        else:
            messages = __order.message_set.order_by("id")
            return redirect(f"/order-a-bot/{_order}", {"order": __order, "messages": messages})