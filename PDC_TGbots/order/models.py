from django.utils import timezone
from django.db import models
from users.models import User
import datetime

class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    order_title = models.CharField("Название заказа", max_length=100)
    order = models.TextField("Заказ")
    power = models.CharField("Сложность", max_length=100)
    order_date = models.DateTimeField("Дата отправки")
    price = models.IntegerField("Цена")
    state = models.CharField("Состояние", max_length=10)
    code = models.FileField("Код")
    def __str__(self):
        return self.order_title
    def is_new_day(self):
        return self.order_date >= (timezone.now() - datetime.timedelta(days = 1))
    def is_new_week(self):
        return self.order_date >= (timezone.now() - datetime.timedelta(days = 7))
    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"

class Message(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    message_text = models.TextField("Текст сообщения")
    #publication_date = models.DateTimeField("Дата отправки")
    def __str__(self):
        return f"заказ: {self.order}, сообщение: {self.message_text}"
    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
