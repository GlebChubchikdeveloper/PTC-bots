from django.contrib import admin
from .models import Order, Message

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    pass
