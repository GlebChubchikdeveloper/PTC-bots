# Generated by Django 4.1 on 2022-10-09 11:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0011_order_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='state',
            field=models.CharField(max_length=10, verbose_name='Состояние'),
        ),
    ]
